# WebSaver

Aplikacja WebSaver, poprzez us�ug� REST, umo�liwia przekazanie URL strony, kt�ra ma zosta� przechowana. Aplikacja pobiera jej kod HTML i wraz z adresem zapisuje w bazie danych. 
Pobieranie stron odbywa si� asynchronicznie wed�ug przyj�tych ��da�. 

Uzyte technologie:
-Java EE
-Jax-RS
-JMS
-JDBC
-MySQL
-JSoup

Aby zapisa� stron� w bazie, u�ytkownik przekazuje jej adres URL (przy pomocy Json) poprzez us�ug� POST. ��danie to trafia na kolejk�, gdzie przez odpowiedni komponent jest obs�ugiwane.
Przy pomocy biblioteki JSoup do aplikacji pobierana jest zawarto�� witryny, a nast�pnie komplet danych zapisywany jest w bazie danych.

Poprzez odpowiednie end-pointy istnieje mo�liwo�� pobrania zasob�w z bazy (w formie listy, pojedynczych zasob�w oraz mo�liwo�� wyszukania zasobu przy u�yciu fragmentu adresu URL).
