package pl.projekt.websaver.dao;

import pl.projekt.websaver.models.Website;

import java.util.List;

public interface WebsiteDao {

    Website createWebsite(String url, String websiteContent);

    List<Website> getAll();

    Website getById(int id);

    List<Website> getByName(String name);
}
