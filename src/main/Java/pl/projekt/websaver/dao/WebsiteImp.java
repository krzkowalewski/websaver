package pl.projekt.websaver.dao;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import pl.projekt.websaver.models.Website;

import java.util.List;

@ApplicationScoped
public class WebsiteImp implements WebsiteDao {

    @PersistenceContext(name = "WebSaver")
    private EntityManager em;

    @Transactional
    public Website createWebsite(String url, String websiteContent) {

        try {
            Website website = new Website(url, websiteContent);
            em.persist(website);
            return website;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Transactional
    public List<Website> getAll() {

        try {
            return em.createQuery("select w from Website w", Website.class).getResultList();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Transactional
    public Website getById(int id) {

        try {
            return em.find(Website.class, id);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Transactional
    public List<Website> getByName(String name) {

        try {
            return em.createQuery("select w from Website w where w.url like :name", Website.class)
                    .setParameter("name", "%" + name + "%")
                    .getResultList();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}