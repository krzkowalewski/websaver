package pl.projekt.websaver.downloadClient;

public interface WebContentClient {

    String downloadWebContent(String url);
}