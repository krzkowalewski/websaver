package pl.projekt.websaver.downloadClient;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import javax.ejb.Stateless;
import java.io.IOException;

@Stateless
public class WebContentImp implements WebContentClient {

    public String downloadWebContent(String url) {
        String webContent = null;
        Connection connect = Jsoup.connect(url);
        Document document;
        try {
            document = connect.get();
            webContent = document.html();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return webContent;
    }

}
