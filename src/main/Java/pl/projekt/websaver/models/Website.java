package pl.projekt.websaver.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;


@Entity
@Table(name = "WEBSAVER")
public class Website {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull
    @Column(name = "URL")
    private String url;

    @Column(name = "SITE")
    @Lob
    private String websiteContent;

    public Website() {
    }

    public Website(String url, String websiteContent) {
        this.url = url;
        this.websiteContent = websiteContent;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getWebsiteContent() {
        return websiteContent;
    }

    public void setWebsiteContent(String websiteContent) {
        this.websiteContent = websiteContent;
    }

    @Override
    public String toString() {
        return "Website{" +
                "id=" + id +
                ", url='" + url + '\'' +
                ", websiteContent='" + websiteContent + '\'' +
                '}';
    }
}
