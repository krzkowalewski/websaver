package pl.projekt.websaver.jms;

public interface JmsFacadeServiceLocal {

    void sendMessage(String url);
}
