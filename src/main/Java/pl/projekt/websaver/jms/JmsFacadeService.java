package pl.projekt.websaver.jms;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.jms.*;

@Stateless
public class JmsFacadeService implements JmsFacadeServiceLocal {

    @Resource(name = "InVmConnectionFactory", mappedName = "java:/ConnectionFactory")
    private ConnectionFactory connectionFactory;
    @Resource(name = "webQueue", mappedName = "java:/jms/queue/webQueue")
    private Queue queue;

    @Override
    public void sendMessage(String url) {
        try {
            Connection connection = connectionFactory.createConnection();
            Session session = connection.createSession(false, QueueSession.AUTO_ACKNOWLEDGE);
            MessageProducer producer = session.createProducer(queue);
            Message message = session.createMapMessage();
            message.setStringProperty("url", url);
            connection.start();
            producer.send(message);
        } catch (JMSException e) {
            e.printStackTrace();
        }

    }
}
