package pl.projekt.websaver.jms;

import pl.projekt.websaver.dao.WebsiteImp;
import pl.projekt.websaver.downloadClient.WebContentClient;
import pl.projekt.websaver.models.Website;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;

@MessageDriven(messageListenerInterface = MessageListener.class,
        activationConfig = {
                @ActivationConfigProperty(propertyName = "destination", propertyValue = "jms/queue/webQueue"),
                @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")
        }
)
public class QueueListener implements MessageListener {

    @EJB
    private WebContentClient service;

    @Inject
    @SuppressWarnings("unused")
    private WebsiteImp dao;

    @Override
    public void onMessage(Message message) {

        try {
            String url = message.getStringProperty("url");
            String websiteContent = service.downloadWebContent(url);
            System.out.println("Service return: " + url);
            Website newWeb = dao.createWebsite(url, websiteContent);

        } catch (JMSException e) {
            e.printStackTrace();
        }
    }
}
