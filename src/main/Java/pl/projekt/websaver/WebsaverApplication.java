package pl.projekt.websaver;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;


@ApplicationPath("/websaver")

public class WebsaverApplication extends Application {
}
