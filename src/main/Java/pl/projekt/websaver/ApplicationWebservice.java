package pl.projekt.websaver;

import pl.projekt.websaver.dao.WebsiteDao;
import pl.projekt.websaver.jms.JmsFacadeServiceLocal;
import pl.projekt.websaver.models.Website;
import javax.ejb.EJB;
import javax.inject.Inject;
import javax.json.JsonObject;
import javax.validation.constraints.Min;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/web")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ApplicationWebservice {

    @Inject
    @SuppressWarnings("unused")
    private WebsiteDao dao;

    @EJB
    private JmsFacadeServiceLocal facade;

    @GET
    public Response listWebsites() {
        return Response.ok(dao.getAll()).build();
    }

    @POST
    public Response addWebsite(JsonObject json) {

        String url = json.getString("url");
        facade.sendMessage(url);

        return Response.ok("Strona: " + url + " została pobrana prawidłowo.").build();
    }

    @GET
    @Path("{id}")
    public Response getWebsiteById(@PathParam("id") @Min(0) Integer id) {
        Website website = dao.getById(id);
        if (website != null) {
            return Response.ok(website).build();
        }
        return Response.status(Response.Status.NOT_FOUND).build();
    }

    @GET
    @Path("/search/{name}")
    public Response getWebsiteContainsTextInUrl(@PathParam("name") String name) {
        List<Website> website = dao.getByName(name);
        if (website != null) {
            return Response.ok(website).build();
        }
        return Response.status(Response.Status.NOT_FOUND).build();
    }

}